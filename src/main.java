import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class main {

    static char[] cbuf = new char[4000];
    static char[][] board = new char[5000][5000]; // pierwszy problem na ktory natrafiam przy tworzeniu tej apki to wielkosc tablic
    static String[] instr = new String[700]; // jeden wir (instrukcje)
    static String[] instr2 = new String[700]; // drugi wir
    static char[][] headOfVortex1 = new char[5000][5000];
    static int headX = 0;
    static int headY = 0;


    public static void main(String args[]) throws IOException {
        File file = new File("puzzle input3.txt");
        BufferedReader br = new BufferedReader((new FileReader(file)));

        int value = 0;
        int i = 0;
        int j = 0;
        int range = 0;
        int length = 0;
        while ((value = br.read()) != -1) { //czytam kazdy znak z pliku

            if (j <= 300) {
                if (value != ',') {
                    instr[j] += (char) value;
                } else j++;
            }

            if (j >= 301) // obliczylem wczesniej petla, ze pierwszy z wirow konczy sie gdy j ma 300 wiec drugi od 301
            {
                if (value != ',') {
                    instr2[i - 1] += (char) value;
                    System.out.println("Ymm: " + (i - 1) + " " + instr2[i - 1]);
                } else i++;
            }
        }
        System.out.println(instr[1].charAt(5)); // to wszystko jest aby sprawdzac co program wyswietla
        System.out.println(instr[300]);
        System.out.println(instr2[0]);

        for (int k = 0; k <= 300; k++) {

            headOfVortex1[0][0] = '*';

            switch (instr[k].charAt(4)) { // pojawia sie wlasnie cos takiego, ze jest null na poczatku kazdego stringa, wiec zaznaczam zeby czytal od 4 literki
               // robie switcha aby wybierac za pomoca jego kierunek poruszania wiru
                case 'U': {
                    length = instr[k].length();
                    range = Integer.parseInt(instr[k].substring(5, length)); // zapisuje inta jako wszystkie znaki po literce tego stringa
                    System.out.println(range);
                    System.out.println("W górę idziesz" + k + " " + range);

                    board[headX][headY + range] = '*'; // wymyslam, ze glowa wiru to gwiazdka

                    for(int l = 0; l <= range; l++)
                    {
                        board[headX][headY + l] = 'X'; // a jego tlow to wieczne X
                    }
                    headY += range;
                    break;
                }

                case 'D': {
                    length = instr[k].length();
                    range = Integer.parseInt(instr[k].substring(5, length));
                    System.out.println(range);
                    System.out.println("W dół idziesz" + k + " " + range);

                    board[headX][headY - range] = '*';

                    for(int l = 0; l <= range; l++)
                    {
                        board[headX][headY - l] = 'X';
                    }
                    headY -= range;
                    break;
                }

                case 'R': {
                    length = instr[k].length();
                    range = Integer.parseInt(instr[k].substring(5, length));
                    System.out.println(range);
                    System.out.println("W prawo idziesz" + k + " " + range);

                    board[headX + range][headY] = '*';

                    for(int l = 0; l <= range; l++)
                    {
                        board[headX + l][headY] = 'X';
                    }
                    headX += range;
                    break;
                }

                case 'L': {
                    length = instr[k].length();
                    range = Integer.parseInt(instr[k].substring(5, length));
                    System.out.println(range);
                    System.out.println("W lewo idziesz" + k + " " + range);

                    board[headX - range][headY] = '*';

                    for(int l = 0; l <= range; l++)
                    {
                        board[headX - l][headY] = 'X';
                    }
                    headX -= range;
                    break;
                }

                default:
                    System.out.println("coś nie pykło" + k);
            }


        }
    }
}
